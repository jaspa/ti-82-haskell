module Main where

import System.IO
import System.Environment
import qualified Data.Text as T
import qualified Data.Text.IO as T

import TI82

main :: IO ()
main = do args <- getArgs
          loop $ if "--debug" `elem` args
                        then debugContext
                        else emptyContext

loop :: Context -> IO ()
loop context = do
    putStr "Input: "
    hFlush stdout

    input <- T.getLine

    if T.null input
        then return ()
        else case runTI82Context context "<stdin>" input of
                Left err -> recurse (err, context)
                Right env -> recurse env

recurse :: Show a => (a, Context) -> IO ()
recurse (val, ctxt) = print val >> loop ctxt
