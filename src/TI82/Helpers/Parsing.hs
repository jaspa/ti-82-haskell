module TI82.Helpers.Parsing where

import Prelude hiding (min, max)
import Data.Functor.Identity

import Text.Parsec.Prim
import Text.Parsec.Combinator

import TI82.Eval.Types

choice' :: [Parsec s u a] -> Parsec s u a
choice' = foldr1 (<|>)

count' :: Stream s m t => (Int, Int) -> ParsecT s u m a -> ParsecT s u m [a]
count' (min, max) p | max < min = return []
                    | otherwise = count min p >>= scan (max - min)
    where scan 0 as = return as
          scan n as = do { a <- p
                         ; scan (n - 1) (as ++ [a])
                         } <|> return as

reduce :: (b -> a -> b) -> b -> Parsec s u a -> Parsec s u b
reduce f x p = do { x' <- p
                  ; reduce f (f x x') p
                  } <|> return x

mapParsec :: Monad m => (a -> m a') -> Parsec s u a -> ParsecT s u m a'
mapParsec f p = mkPT $ \s -> mapConsumed $ runIdentity $ runParsecT p s
    where mapConsumed (Consumed a) = return . Consumed . mapReply $ runIdentity a
          mapConsumed (Empty a)    = return . Empty    . mapReply $ runIdentity a
          mapReply (Ok a s' e) = (\a' -> Ok a' s' e) <$> f a
          mapReply (Error e)   = return $ Error e

liftErr :: Parser a -> ErrParser a
liftErr = mapParsec return
