module TI82.Helpers.ErrorAcc (ErrAcc(..)) where

newtype ErrAcc a b = ErrAcc { unErrAcc :: Either a b }

instance Functor (ErrAcc a) where
    fmap f = ErrAcc . fmap f . unErrAcc

instance Monoid a => Applicative (ErrAcc a) where
    pure = ErrAcc . Right
    x <*> y = ErrAcc $ case (unErrAcc x, unErrAcc y) of
                (Right f, Right v) -> Right (f v)
                (Left e1, Left e2) -> Left (e1 `mappend` e2)
                (Left e, _) -> Left e
                (_, Left e) -> Left e
