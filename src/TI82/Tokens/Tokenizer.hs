{-# LANGUAGE OverloadedStrings #-}

module TI82.Tokens.Tokenizer
  ( module TI82.Tokens.Token
  , tokenizer
  ) where

import qualified Data.Text as T
import Text.Parsec
import Text.Parsec.Text

import TI82.Tokens.Token
import TI82.Helpers.Parsing

type Tokenizer = Parser Token

-- Helper functions

class Textable a where
    asText :: a -> T.Text
    asParser :: a -> Parser T.Text

instance Textable T.Text where
    asText = id
    asParser t = t <$ string (T.unpack t)

instance Textable Char where
    asText = T.singleton
    asParser = fmap T.singleton . char

infix 3 @=, @:, @~

makeToken :: TokenKind -> Parser T.Text -> Tokenizer
makeToken kind parser = do state <- getParserState
                           source <- parser
                           return $ newToken kind source $ statePos state

(@=) :: Textable a => TokenKind -> Parser a -> Tokenizer
(@=) kind def = makeToken kind $ asText <$> def

(@:) :: Textable a => TokenKind -> a -> Tokenizer
(@:) kind def = makeToken kind $ asParser def

(@~) :: TokenKind -> T.Text -> Tokenizer
(@~) = (@:)

text :: T.Text -> Parser T.Text
text = asParser

-- Tokenizer spec

tokenizer :: Parser [Token]
tokenizer = do ts <- many1 $ choice' [
                  tEOS, tDecimal, tOperator, tLitDelims,
                  tVarRef, tGrouping, tStoreArr, tUnknown ]
               t <- TKEOS @= T.empty <$ eof
               return $ ts ++ [t]
  where
    tEOS = TKEOS @= (char ':' <|> endOfLine)

    tDecimal = TKDigit @= digit <|> TKPoint @: '.'

    tOperator = choice' [ TKPlus @: '+', TKMinus @: '-'
                        , TKMul @: '*', TKDiv @: '/'
                        , TKSquare @: '²', TKCube @: '³'
                        , TKRecip @~ "⁻¹", TKFact @: '!' ]

    tLitDelims = choice' [ TKListOpen @: '{', TKListClose @: '}'
                         , TKQuote @: '"' ]

    tVarRef = choice' [ TKVarAns @= try (text "Ans")
                      , TKVarList @= lst
                      , TKVarString @= str
                      , TKVarNum @= ti82Letter ]
      where str = try (text "Str") >> T.snoc "Str" <$> digit
            lst = char 'l' >> T.cons 'l' <$> (listLong <|> listShrt)
            listLong = do l  <- ti82Letter
                          ls <- count' (0, 4) (ti82Letter <|> digit)
                          return $ T.pack (l:ls)
            listShrt = T.singleton <$> (oneOf "123456" :: Parser Char)

    tGrouping = choice' [ TKParenOpen @: '(', TKParenClose @: ')'
                        , TKComma @: ',' ]

    tStoreArr = TKStoreArr @: '→'

    tUnknown = TKUnknown @= anyChar

    ti82Letter = satisfy $ \c -> 'A' <= c && 'Z' >= c || c == 'Θ'
