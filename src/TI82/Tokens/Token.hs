module TI82.Tokens.Token
  ( TokenKind(..)
  , Token()
  , newToken
  , tokenKind
  , tokenInfo
  , tokenSource
  , tokenStart
  , tokenEnd
  , tokenRange
  ) where

import qualified Data.Text as T
import Text.Parsec.Pos

import TI82.Tokens.Annotated

data TokenKind = TKEOS | TKUnknown
               | TKPlus | TKMinus | TKMul | TKDiv
               | TKSquare | TKCube | TKRecip | TKFact
               | TKDigit | TKPoint
               | TKListOpen | TKListClose | TKQuote
               | TKVarAns | TKVarNum | TKVarList | TKVarString
               | TKParenOpen | TKParenClose | TKComma | TKStoreArr
    deriving (Eq, Show)

newtype Token = Token { getAnn :: Annotated TokenKind }

newToken :: TokenKind -> T.Text -> SourcePos -> Token
newToken kind src pos = Token $ Annotated kind $ makeSourceInfo src pos

tokenKind :: Token -> TokenKind
tokenKind = annValue . getAnn

tokenInfo :: Token -> SourceInfo
tokenInfo = annInfo . getAnn

tokenSource :: Token -> T.Text
tokenSource = source . tokenInfo

tokenStart, tokenEnd :: Token -> SourcePos
tokenStart = rangeStart . tokenRange
tokenEnd = rangeEnd . tokenRange

tokenRange :: Token -> SrcRange
tokenRange = srcRange . tokenInfo

instance Show Token where
    show t = concat [ "<"
                    , show $ tokenKind t
                    , " ~ "
                    , show $ tokenSource t
                    , " @"
                    , show $ tokenStart t
                    , ">"
                    ]
