module TI82.Tokens.Annotated
  ( module Text.Parsec.Pos
  , SrcRange(..)
  , rangeStart
  , rangeEnd
  , SourceInfo(..)
  , makeSourceInfo
  , Annotated(..)
  , annotate
  , expand
  , (+>)
  ) where

import qualified Data.Text as T
import Data.List (intercalate)
import Text.Parsec.Pos

newtype SrcRange = SrcRange (SourcePos, SourcePos)
    deriving (Eq)

rangeStart, rangeEnd :: SrcRange -> SourcePos
rangeStart (SrcRange (p,_)) = p
rangeEnd   (SrcRange (_,p)) = p

instance Show SrcRange where
    show (SrcRange (a,b)) =
        let (lnA, lnB) = (sourceLine a, sourceLine b)
            (clA, clB) = (sourceColumn a, sourceColumn b - 1)
            start      = intercalate ":" [sourceName a, show lnA, show clA]
         in start ++ " - " ++ show lnB ++ ":" ++ show clB

data SourceInfo = SourceInfo { source :: T.Text
                             , srcRange :: SrcRange
                             } deriving (Eq, Show)

data Annotated a = Annotated { annValue :: a
                             , annInfo :: SourceInfo
                             } deriving (Eq, Show)

instance Functor Annotated where
    fmap f ann = ann { annValue = f $ annValue ann }

makeSourceInfo :: T.Text -> SourcePos -> SourceInfo
makeSourceInfo t pos = let range = (pos, updatePosString pos $ T.unpack t)
                        in SourceInfo t $ SrcRange range

annotate :: SourceInfo -> a -> Annotated a
annotate = flip Annotated

expandLoc :: SrcRange -> SrcRange -> SrcRange
expandLoc r1 r2
    | rangeEnd r1 /= rangeStart r2 =
            let info = show r1 ++ "\n   +>\n" ++ show r2
                err  = "Annotated.expandLoc: incompatible ranges\n"
             in error $ err ++ info
    | otherwise = SrcRange (rangeStart r1, rangeEnd r2)

expand :: SourceInfo -> SourceInfo -> SourceInfo
expand (SourceInfo s1 r1) (SourceInfo s2 r2)
    -- Force evaluation, so `expandLoc` errors bubble up!
    = let newLoc = expandLoc r1 r2
       in newLoc `seq` SourceInfo (T.append s1 s2) newLoc

infixl 6 +>

(+>) :: SourceInfo -> SourceInfo -> SourceInfo
(+>) = expand
