{-# LANGUAGE OverloadedStrings #-}

module TI82.Eval.Value
  ( Value(..), ValueType(..)
  , makeValueList
  , showType
  , valueType
  ) where

import qualified Data.Text as T
import Data.Monoid ((<>))
import Control.Applicative (liftA2)

import TI82.Eval.Error
import TI82.Helpers.ErrorAcc

data Value = Number Double
           | String [T.Text]
           | List [Double]
    deriving (Eq, Show)

data ValueType = VTNum | VTList | VTStr
    deriving (Eq)

instance Show ValueType where
    show VTNum  = "NUM"
    show VTList = "LST"
    show VTStr  = "STR"

valueType :: Value -> ValueType
valueType (Number _) = VTNum
valueType (List   _) = VTList
valueType (String _) = VTStr

showType :: Value -> T.Text
showType = T.pack . show . valueType

makeValueList :: [Annotated Value] -> Either EvalError Value
makeValueList =
    fmap List . unErrAcc . foldr (liftA2 (:) . ErrAcc . num) (pure [])
  where num (Annotated (Number n) _) = Right n
        num (Annotated v info)       =
          let e = "Lists can only contain NUM values, this value has type "
                    <> showType v <> "."
           in Left $ EvalError (DataType e) info
