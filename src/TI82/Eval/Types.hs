module TI82.Eval.Types
  ( module TI82.Tokens.Token
  , module TI82.Tokens.Annotated
  , Value, EvalError(..), Context
  , Parser, AnnP, P
  , ErrParser, ErrAnnP,  ErrP
  , throwError
  ) where

import Text.Parsec.Prim
import Control.Monad.Except (Except, throwError)

import TI82.Tokens.Token
import TI82.Tokens.Annotated
import TI82.Eval.Value (Value)
import TI82.Eval.Error (EvalError(..))
import TI82.Eval.Context (Context)

type Parser = Parsec [Token] Context
type AnnP a = Parser (Annotated a)
type P      = AnnP Value

type ErrParser = ParsecT [Token] Context (Except EvalError)
type ErrAnnP a = ErrParser (Annotated a)
type ErrP      = ErrAnnP Value
