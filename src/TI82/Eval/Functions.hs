{-# LANGUAGE OverloadedStrings, ForeignFunctionInterface #-}

module TI82.Eval.Functions
  ( ErrT, UnNumF, BinNumF
  , negate, sq, cu, fact, recip
  , add, sub, mul, div
  ) where

import Prelude hiding (negate, recip, div)
import qualified Prelude (negate, recip)
import Foreign.C.Types (CDouble(..))
import Data.Foldable

import TI82.Eval.Error (ErrorSpec(..))

type ErrT = Either ErrorSpec
type UnNumF = Double -> ErrT Double
type BinNumF = Double -> UnNumF

safe' :: UnNumF
safe' = Right

safe :: (Double -> Double) -> UnNumF
safe f a = Right $ f a

safe2 :: (Double -> Double -> Double) -> BinNumF
safe2 f a b = Right $ f a b

-- Definitions

negate, sq, cu, fact, recip :: UnNumF
negate = safe Prelude.negate
sq a = safe' (a*a)
cu a = safe' (a*a*a)

fact 0 = safe' 1
fact a | a < 0        = Left $ Domain "negative number for factorial()"
       | isFloating a = Left $ Domain "factorial() is only defined for integers"
       | otherwise    = safe' $ foldr' (*) 1 [1..a]

recip 0 = Left DivideBy0
recip a = safe' $ Prelude.recip a

add, sub, mul, div :: BinNumF
add = safe2 (+)
sub = safe2 (-)
mul = safe2 (*)
div _ 0 = Left DivideBy0
div a b = safe2 (/) a b

-- Helpers

isFloating :: Double -> Bool
isFloating d = case c_rint (CDouble d) of CDouble d' -> d /= d'

foreign import ccall unsafe "math.h rint" c_rint :: CDouble -> CDouble
