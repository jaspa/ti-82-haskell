module TI82.Eval.Prim
  ( module Text.Parsec.Prim
  , module Text.Parsec.Combinator
  , module TI82.Eval.Types
  , module TI82.Helpers.Parsing
  , pIs, pIs2, pNot, pNot'
  ) where

import Text.Parsec.Prim
import Text.Parsec.Combinator
import TI82.Eval.Types
import TI82.Helpers.Parsing

pIs :: TokenKind -> Parser Token
pIs k = satisfy (==k) <?> show k

pIs2 :: (TokenKind, TokenKind) -> Parser Token
pIs2 (k1,k2) = satisfy $ \k -> k == k1 || k == k2

pNot :: TokenKind -> Parser Token
pNot k = satisfy (/=k)

pNot' :: [TokenKind] -> Parser Token
pNot' ks = satisfy $ not . flip elem ks

satisfy :: (TokenKind -> Bool) -> Parser Token
satisfy f = tokenPrim show
                      (\_ t _ -> tokenEnd t)
                      (\t -> if f (tokenKind t) then Just t else Nothing)
