{-# LANGUAGE OverloadedStrings, TupleSections #-}

module TI82.Eval.Parser
  ( module TI82.Eval.Value
  , module TI82.Eval.Error
  , evalParser
  ) where

import qualified Data.Text as T

import Data.Char (digitToInt)
import Data.Foldable (foldl')
import Data.Function
import Data.Monoid ((<>))

import TI82.Eval.Prim
import TI82.Eval.Expr
import TI82.Eval.Value
import TI82.Eval.Error
import TI82.Eval.Context
import TI82.Helpers.Functor

-- Parsers

eos :: Parser Token
eos = pIs TKEOS

optClose :: Parser Token -> Parser Token
optClose = lookAhead . (<|> try eosNL)
  where eosNL = do t <- eos
                   if tokenSource t /= ":"
                      then return t
                      else unexpected "inline EOS"

number :: P
number = let intVal = digitToInt . T.head . tokenSource

             digit = pIs TKDigit

             intPart = do dig <- digit
                          let acc0 = (intVal dig, tokenInfo dig)
                          uncurry Annotated <$> reduce f acc0 digit
               where f (v,i) dig = (v * 10 + intVal dig, i +> tokenInfo dig)

             fractionPart :: Bool -> AnnP Double
             fractionPart req =
                 do point <- pIs TKPoint
                    digs <- (if req then many1 else many) digit

                    makeRes $ case reverse digs of
                        dig:digs' ->
                            let start = fromIntegral (intVal dig) / 10
                                (v,i) = foldl' f (start, tokenInfo dig) digs'
                             in (v, tokenInfo point +> i)

                        [] -> (0, tokenInfo point)

               where f (v,i) dig =
                        let v' = (v + fromIntegral (intVal dig)) / 10
                            i' = tokenInfo dig +> i
                         in v' `seq` i' `seq` (v',i')

                     makeRes = return . uncurry Annotated

             full = do int <- fromIntegral <$$> intPart
                       frac <- optionMaybe $ fractionPart False
                       return $ case frac of
                           Just frac' -> Annotated (((+) `on` annValue) int frac')
                                                   (((+>) `on` annInfo) int frac')
                           Nothing -> int

         in Number <$$> (full <|> fractionPart True) <?> "Number"

string :: P
string = let quote = pIs TKQuote
             anyT  = pNot TKUnknown

          in do q1 <- quote
                body <- manyTill anyT $ optClose quote
                q2 <- optionMaybe quote

                let info = foldl' (\i t -> i +> tokenInfo t)
                                  (tokenInfo q1)
                                  body

                return $ Annotated (String $ tokenSource <$> body)
                                   (maybe info (\q -> info +> tokenInfo q) q2)

list :: ErrP
list = let close = (, False) <$> pIs TKListClose
                    <|> (, True) <$> lookAhead eos

        in do bL <- liftErr $ pIs TKListOpen
              vs <- sepBy1 expression $ liftErr $ pIs TKComma
              (bR, isEOS) <- liftErr close

              case makeValueList vs of
                Left e -> throwError e
                Right list' ->
                    let src = if isEOS then "{ ..." else "{ ... }" :: T.Text
                        loc = SrcRange (tokenStart bL, tokenEnd bR)
                     in return $ Annotated list' $ SourceInfo src loc

varRefDetector :: AnnP (T.Text, Maybe ValueType)
varRefDetector = do (tok, ty) <- choice' $ makeP <$> types
                    return $ Annotated (tokenSource tok, ty) (tokenInfo tok)
  where types = [ (TKVarAns, Nothing), (TKVarNum, Just VTNum)
                , (TKVarList, Just VTList), (TKVarString, Just VTStr) ]
        makeP (k,t) = (, t) <$> pIs k

varRef :: ErrP
varRef = do ref <- liftErr varRefDetector
            ctxt <- getState

            let (name, ty) = annValue ref
                info = annInfo ref

            annotate info <$> if maybe True (== VTNum) ty
              then return $ lookupNumber name ctxt
              else case lookupVar name ctxt of
                     Nothing -> throwError $
                        EvalError (Undefined "variable undefined") info
                     Just v -> return v

storeArrow :: Value -> ErrParser ()
storeArrow val =
    do ref <- liftErr $ pIs TKStoreArr >> varRefDetector

       let (name, ty) = annValue ref
           info = annInfo ref
           valT = valueType val

       case ty of
         Nothing -> throwError $ ansErr info
         Just ty' | ty' /= valT -> throwError $ typeErr valT ty' info
                  | otherwise -> modifyState (updateVar name val)

  where ansErr = EvalError (Syntax "can't store into the Ans variable")
        typeErr tVal tVar = EvalError
            (DataType $ "can't store value of type " <> T.pack (show tVal) <>
                          " into variable of type " <> T.pack (show tVar))

parentheses :: ErrP
parentheses = do pL <- liftErr $ pIs TKParenOpen
                 expr <- expression
                 pR <- liftErr $  pIs TKParenClose <|> lookAhead eos
                 return $ Annotated (annValue expr) $
                    tokenInfo pL +> annInfo expr +> tokenInfo pR

operand :: ErrP
operand = opParser $ literal <|> varRef <|> parentheses
    where literal = liftErr (number <|> string) <|> list

expression :: ErrP
expression = exprParser operand

node :: ErrP
node = do e <- expression
          optional . storeArrow $ annValue e
          modifyState (setAnsValue $ annValue e)
          return e

-- EvalParser spec

evalParser :: ErrParser [Value]
evalParser = annValue <$$> endBy1 node (liftErr eos) <* liftErr eof
