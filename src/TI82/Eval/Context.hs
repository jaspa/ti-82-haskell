{-# LANGUAGE OverloadedStrings #-}

module TI82.Eval.Context
  ( Context(..)
  , lookupVar
  , lookupNumber
  , updateVar
  , ansValue
  , setAnsValue
  , emptyContext
  , debugContext
  ) where

import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import qualified Data.HashMap.Lazy as M

import TI82.Eval.Value

type VarTable = M.HashMap T.Text Value

data Context = Context { variables :: VarTable
                       , complexLiterals :: Bool
                       } deriving (Eq, Show)

lookupVar :: T.Text -> Context -> Maybe Value
lookupVar var = M.lookup var . variables

lookupNumber :: T.Text -> Context -> Value
lookupNumber var = fromMaybe (Number 0) . lookupVar var

updateVar :: T.Text -> Value -> Context -> Context
updateVar name val ctx = ctx { variables = M.insert name val $ variables ctx }

ansValue :: Context -> Value
ansValue = lookupNumber "Ans"

setAnsValue :: Value -> Context -> Context
setAnsValue = updateVar "Ans"

emptyContext :: Context
emptyContext = Context { variables = M.empty, complexLiterals = True }

debugContext :: Context
debugContext = emptyContext { variables = M.fromList list }
    where list = [ ("A", Number 3.1415)
                 , ("l1", List [0.1, 0.2, 0.5]), ("lABC", List [1.1, 2.2, 5.5])
                 , ("Str1", String ["H", "A", "L", "O", "!"]) ]
