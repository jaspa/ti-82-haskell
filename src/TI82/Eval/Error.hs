module TI82.Eval.Error
  ( ErrorSpec(..)
  , EvalError(..)
  , Annotated(..)
  ) where

import qualified Data.Text as T
import Data.List
import TI82.Tokens.Annotated

data ErrorSpec = DataType T.Text
               | DimMismatch T.Text
               | DivideBy0
               | Domain T.Text
               | Syntax T.Text
               | Undefined T.Text
    deriving (Eq)

data EvalError = EvalError ErrorSpec SourceInfo
               | ErrorList [EvalError]
    deriving (Eq)

instance Show ErrorSpec where
    show (DataType    t) = "ERROR:DATA TYPE (" ++ T.unpack t ++ ")"
    show (DimMismatch t) = "ERROR:DIM MISMATCH (" ++ T.unpack t ++ ")"
    show DivideBy0       = "ERROR:DIVIDE BY 0"
    show (Domain      t) = "ERROR:DOMAIN (" ++ T.unpack t ++ ")"
    show (Syntax      t) = "ERROR:SYNTAX (" ++ T.unpack t ++ ")"
    show (Undefined   t) = "ERROR:UNDEFINED (" ++ T.unpack t ++ ")"

instance Show EvalError where
    show (EvalError spec info) =
        show (srcRange info) ++ ": " ++ show spec ++
            "\n    " ++ T.unpack (source info)
    show (ErrorList es) = intercalate "\n\n" . map show $ flatten es

instance Monoid EvalError where
    mempty = ErrorList []

    mappend (ErrorList es1) (ErrorList es2) = ErrorList $ es1 ++ es2
    mappend (ErrorList es)  e               = ErrorList $ es ++ [e]
    mappend e               (ErrorList es)  = ErrorList $ e:es
    mappend e1              e2              = ErrorList [e1, e2]

    mconcat = ErrorList . flatten

flatten :: [EvalError] -> [EvalError]
flatten = foldr f []
    where f (ErrorList es) = (flatten es ++)
          f e = (e:)
