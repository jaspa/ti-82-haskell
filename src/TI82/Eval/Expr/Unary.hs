{-# LANGUAGE OverloadedStrings #-}

module TI82.Eval.Expr.Unary (opParser) where

import qualified Data.Text as T
import Data.Monoid ((<>))

import TI82.Eval.Prim
import TI82.Eval.Value
import TI82.Eval.Error

import TI82.Eval.Functions (UnNumF, ErrT)
import qualified TI82.Eval.Functions as F

type UnaryF = Value -> ErrT Value

opParser :: ErrP -> ErrP
opParser = prefixP

prefixP :: ErrP -> ErrP
prefixP simple = do { f <- prefix
                    ; x <- prefixP simple
                    ; apply (+>) f x
                    } <|> operandP simple

    where prefix = unaryOp TKMinus F.negate

operandP :: ErrP -> ErrP
operandP simple = simple >>= postfixP

postfixP :: Annotated Value -> ErrP
postfixP value = do mf <- optionMaybe postfix
                    case mf of
                        Nothing -> return value
                        Just f -> apply (flip (+>)) f value >>= postfixP

    where postfix = unaryOp TKSquare F.sq <|> unaryOp TKCube F.cu <|>
                    unaryOp TKFact F.fact <|> unaryOp TKRecip F.recip

apply :: (SourceInfo -> SourceInfo -> SourceInfo)
            -> Annotated UnaryF -> Annotated Value -> ErrP
apply con f v = let info = con (annInfo f) (annInfo v)
                 in either (throwError . flip EvalError info)
                           (return . annotate info) $
                           annValue f (annValue v)

unaryOp :: TokenKind -> UnNumF -> ErrAnnP UnaryF
unaryOp k f =
    Annotated (unOp' k f) . tokenInfo <$> liftErr (pIs k) <?> "postfix"

unOp' :: TokenKind -> UnNumF -> UnaryF
unOp' _ f (Number x) = Number <$> f x
unOp' _ f (List xs)  = fmap List $ sequence $ f <$> xs
unOp' k _ x = Left . DataType $
    "invalid type " <> showType x <> " for operation `" <> k' <> "`"
  where k' = T.pack $ show k
