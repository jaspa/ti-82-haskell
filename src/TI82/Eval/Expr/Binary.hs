{-# LANGUAGE OverloadedStrings #-}

module TI82.Eval.Expr.Binary (exprParser) where

import qualified Data.Text as T
import Data.Monoid ((<>))

import TI82.Eval.Prim
import TI82.Eval.Value
import TI82.Eval.Error

import TI82.Eval.Functions (BinNumF, ErrT)
import qualified TI82.Eval.Functions as F

type BinF = Value -> Value -> ErrT Value

table :: [[(Parser Token, BinF)]]
table =
    let plus _ (String s1) (String s2) = Right . String $ s1 ++ s2
        plus k a b = binOp k F.add a b
        plusOp = (pIs TKPlus, plus TKPlus)

        ops = [TKPlus, TKMinus, TKMul, TKDiv]
        terms = [TKEOS, TKListClose, TKParenClose]
        impMul = do t <- lookAhead $ pNot' (terms ++ ops)
                    return $ newToken TKMul "" (tokenStart t)
        impMulOp = (impMul <?> "foo", binOp TKMul F.mul)

     in [ [ op TKMul F.mul, op TKDiv F.div, impMulOp]
        , [ plusOp, op TKMinus F.sub ]
        ]

  where op k f = (pIs k, binOp k f)

binOp :: TokenKind -> BinNumF -> BinF
binOp _ f (Number a) (Number b) = Number <$> f a b

binOp _ f (Number a) (List bs) = fmap List $ sequence $ (a `f`) <$> bs
binOp _ f (List as) (Number b) = fmap List $ sequence $ (`f` b) <$> as

binOp _ f (List as) (List bs)
    | length as == length bs = fmap List $ sequence $ uncurry f <$> zip as bs
    | otherwise =
        Left . DimMismatch $ T.pack (show $ length as) <> " vs " <>
                                T.pack (show $ length bs) <> " elements"

binOp k _ a b =
    Left . DataType $ "›" <> ta <> " " <> k' <> " " <> tb <> "‹ is not defined"
  where (ta,tb) = (showType a, showType b); k' = T.pack $ show k

exprParser :: ErrP -> ErrP
exprParser simple = foldl makeParser simple table
  where
    makeParser term operators = let
        ops = choice' $ fmap (\o -> do t <- fst o
                                       return $ Annotated (snd o) (tokenInfo t)
                             ) operators

        rhs x = do
            f <- liftErr ops
            y <- term

            let info = annInfo x +> annInfo f +> annInfo y

            v <- either (throwError . flip EvalError info)
                        (return . annotate info) $
                        annValue f (annValue x) (annValue y)
            rhs' v

        rhs' x = rhs x <|> return x

        in term >>= \x -> rhs x <|> return x <?> "operator"
