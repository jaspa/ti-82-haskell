module TI82
  ( module TI82.Eval.Value
  , module TI82.Eval.Error
  , module Text.Parsec.Pos
  , module Text.Parsec.Error
  , runTI82
  , runTI82Context
  , Context
  , emptyContext
  , debugContext
  ) where

import qualified Data.Text as T
import Text.Parsec.Prim (parse, runParserT, getState)
import Text.Parsec.Error
import Text.Parsec.Pos
import Control.Monad.Except

import TI82.Tokens.Tokenizer
import TI82.Eval.Parser
import TI82.Eval.Value
import TI82.Eval.Error
import TI82.Eval.Context

data TI82Error = Static ParseError
               | Runtime EvalError
    deriving (Eq, Show)

runTI82 :: SourceName -> T.Text -> Either TI82Error [Value]
runTI82 name src = fst <$> runTI82Context emptyContext name src

runTI82Context ::
    Context -> SourceName -> T.Text -> Either TI82Error ([Value], Context)
runTI82Context context name source =
    case parse tokenizer name source of
        Left err -> Left $ Static err
        Right toks ->
            case runExcept $ runParserT eval context name toks of
                -- :: Either EvalError (Either ParseError (Value, Context))
                Left err -> Left $ Runtime err
                Right (Left err) -> Left $ Static err
                Right (Right res) -> Right res

  where eval = do value <- evalParser
                  context' <- getState
                  return (value, context')
